/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, TITLE2, TITLE3, OPTIONS, OPTIONS2, OPTIONS3, OPTIONS4, GAMEPLAY, GAMEPLAY2, SURVIVAL, HISCORES, PAUSE, PAUSE2, PAUSE3, ENDING, ENDING2, ENDING3, ENDING4, ENDING5, ENDING6, ENDING7} GameScreen;

int main()
{
    // Initialization
    //---------------------------------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = LOGO;
    
    bool sound = true;
    int sonido = 0;
    
    int choques = 0;
    int puntuacion = 0;
    int puntuacionfinal = 0;
    
    int puntuacion1 = 0;
    int puntuacion2 = 0;
    int puntuacion3 = 0;
    
    int limitesia = screenWidth/2;
    
    int secondsCounter = 99;
    
    int framesCounter = 0;
    int framesCounter2 = 0;
    
    InitWindow(screenWidth, screenHeight, windowTitle);//---------------------INITWINDOW----------------------------
    InitAudioDevice();
    
    //------------------------------------------------------------------------SONIDO--------------------------------
    Sound inicio = LoadSound("resources/inicio.wav");
    Sound seleccion = LoadSound("resources/seleccion.wav");
    Sound moneda = LoadSound("resources/moneda.wav");
    Sound rebote = LoadSound("resources/rebote.wav");
    Sound vida = LoadSound("resources/vida.wav");
    Sound powerup = LoadSound("resources/powerup.wav");
    Music music = LoadMusicStream("resources/music.ogg");
    //------------------------------------------------------------------------LOGO----------------------------------
    Image image = LoadImage("resources/logo.png");
    Texture2D texture2 = LoadTextureFromImage(image);
    
    //------------------------------------------------------------------------TITLE---------------------------------
    Texture2D texture = LoadTexture("resources/titulo.png");
    const char message[128] = "PRESS ENTER TO CONTINUE";
    
    //Textura de fondo
    Rectangle rectext;
    rectext.width = 100;
    rectext.height = screenHeight;
    rectext.x = 0;
    rectext.y = 0;
    
    Rectangle rectext2;
    rectext2.width = 100;
    rectext2.height = screenHeight;
    rectext2.x = screenWidth - 100;
    rectext2.y = 0;
    
    Rectangle rectext3;
    rectext3.width = 80;
    rectext3.height = screenHeight;
    rectext3.x = 10;
    rectext3.y = 0;
    
    Rectangle rectext4;
    rectext4.width = 80;
    rectext4.height = screenHeight;
    rectext4.x = screenWidth - rectext.width + 10;
    rectext4.y = 0;
    
    //Tapar textura inicio
    Rectangle rectext5;
    rectext5.width = 100;
    rectext5.height = screenHeight;
    rectext5.x = 0;
    rectext5.y = 0;
    
    Rectangle rectext6;
    rectext6.width = 100;
    rectext6.height = screenHeight;
    rectext6.x = screenWidth - 100;
    rectext6.y = 0;
    
    //Elegir Empezar
    Rectangle rec2;
    rec2.width = screenWidth/2;
    rec2.height = screenHeight/2;
    rec2.x = screenWidth/4;
    rec2.y = screenHeight - screenHeight/3;
    
    //------------------------------------------------------------------------TITLE2---------------------------------
    //Elegir 1 VS 1 o VS IA
    Rectangle rec1;
    rec1.width = 160;
    rec1.height = 32;
    rec1.x = screenWidth/2 - 58;
    rec1.y = 265;
    
    //-----------------------------------------------------------------------GAMEPLAY--------------------------------
    //Tapar controles
    Rectangle recgame1;
    recgame1.width = 170;
    recgame1.height = 230;
    recgame1.x = screenWidth;
    recgame1.y = 140;
    
    Rectangle recgame2;
    recgame2.width = 160;
    recgame2.height = 230;
    recgame2.x = screenWidth;
    recgame2.y = 140;
    
    //pala1 VS IA
    Rectangle pala1;
    pala1.width = 15;
    pala1.height = 70;
    pala1.x = 20;
    pala1.y = screenHeight/2 - pala1.height/2 + 15;
    
    //pala2
    Rectangle pala2;
    pala2.width = 15;
    pala2.height = 70;
    pala2.x = screenWidth - 35;
    pala2.y = screenHeight/2 - pala2.height/2 + 15;
    
    //pala survival
    Rectangle pala3;
    pala3.width = 15;
    pala3.height = 70;
    pala3.x = screenWidth - 35;
    pala3.y = screenHeight/2 - pala3.height/2 + 15;
    
    //Velocidad de las palas
    float velocidad = 3.5f;
    float velocidad2 = 3.5f;
    float velocidadia = 4.0f;
    
    //Lineas
    Rectangle linea;
    linea.width = 6;
    linea.height = screenHeight - 41;
    linea.x = screenWidth/2-linea.width/2;
    linea.y = 41;
    
    Rectangle linea2;
    linea2.width = screenWidth;
    linea2.height = 6;
    linea2.x = 0;
    linea2.y = 35;
    
    Vector2 campo = {screenWidth/2,screenHeight/2 + 20};
    float campoSize = 170.0f;
    Vector2 campo2 = {screenWidth/2,screenHeight/2 + 20};
    float campo2Size = 163.0f;
     
    //Bola
    Vector2 ball = {screenWidth/2,screenHeight/2};
    float ballSize = 15.0f;
   
    float maxVelocity = 5;
   
    Vector2 ballVelocity;
   
    ballVelocity.x = -maxVelocity;
    ballVelocity.y = -maxVelocity;
    
    //Barra vida
    Rectangle rec4;
    rec4.width = 240;
    rec4.height = 35;
    rec4.x = screenWidth/8 + 20;
    rec4.y = 0;
    
    Rectangle rec3;
    rec3.width = 230;
    rec3.height = 30;
    rec3.x = screenWidth/8 + 25;
    rec3.y = 5;
    
    Rectangle rec7;
    rec7.width = 230;
    rec7.height = 30;
    rec7.x = screenWidth/8 + 25;
    rec7.y = 5;
    
    Rectangle rec8;
    rec8.width = 230;
    rec8.height = 30;
    rec8.x = screenWidth/2 + 45;
    rec8.y = 5;
    
    //Barra vida2
    Rectangle rec5;
    rec5.width = 240;
    rec5.height = 35;
    rec5.x = screenWidth/2 + 40;
    rec5.y = 0;
    
    Rectangle rec6;
    rec6.width = 230;
    rec6.height = 30;
    rec6.x = screenWidth/2 + 45;
    rec6.y = 5;
    
    //Powerups
    Rectangle rec9;
    rec9.width = 11;
    rec9.height = 0;
    rec9.x = 22;
    rec9.y = screenHeight/2 - pala1.height/2 + 15;
    
    Rectangle rec10;
    rec10.width = 11;
    rec10.height = 0;
    rec10.x = screenWidth - 33;
    rec10.y = screenHeight/2 - pala1.height/2 + 15;
    
    Rectangle rec11;
    rec11.width = 11;
    rec11.height = 0;
    rec11.x = screenWidth - 33;
    rec11.y = screenHeight/2 - pala1.height/2 + 15;
    
    //------------------------------------------------------------------------HISCORES---------------------------------
    Rectangle rech;
    rech.width = 100;
    rech.height = 32;
    rech.x = screenWidth/2 - 62;
    rech.y = 357;
    
    //------------------------------------------------------------------------OPTIONS----------------------------------
    Rectangle reco;
    reco.width = 200;
    reco.height = 36;
    reco.x = screenWidth/2 - 95;
    reco.y = 155;
    
    Rectangle recs;
    recs.width = 68;
    recs.height = 36;
    recs.x = screenWidth/2 - 118;
    recs.y = 205;
    
    Rectangle recd;
    recd.width = 138;
    recd.height = 36;
    recd.x = 165;
    recd.y = 305;
    
    //-----------------------------------------------------------------------PAUSE------------------------------------
    Rectangle recp;
    recp.width = 162;
    recp.height = 32;
    recp.x = screenWidth/2 - 72;
    recp.y = 217;
    
    //-----------------------------------------------------------------------ENDING-----------------------------------
    Rectangle rece;
    rece.width = 160;
    rece.height = 36;
    rece.x = screenWidth/2 - 75;
    rece.y = 196;
    
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    Color recColor = WHITE;
   
    bool fadeOut = true;
    float alpha = 0;
    float fadeSpeed = 0.01f;
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------   
        UpdateMusicStream(music);
        
        switch(screen) 
        {
            case LOGO: // ------------------------------------------------------------------------------------------------------
            {
            framesCounter++;
            if(framesCounter < 150)
            {
                alpha += fadeSpeed;

                if(alpha >= 1.0f)
                {
                    alpha = 1.0f;
                }
            }
            else
            {
                alpha -= fadeSpeed;
                if(alpha <= 0.0f)
                {
                    alpha = 0.0f;
                }
            }

            if(framesCounter > 150)
            {
                fadeOut = !fadeOut;
            }
            if(framesCounter > 250)
            {
                screen = TITLE;
                alpha = 0;
                framesCounter = 0;
            }
            if(IsKeyPressed(KEY_ENTER))                
            {
                screen = TITLE;
                alpha = 0;
                framesCounter = 0;
            }
            
            } break;
            
            case TITLE: //PRESS ENTER -----------------------------------------------------------------------------------------------
            {
                if(screen == TITLE)
                {
                    alpha += fadeSpeed;

                    if(alpha >= 1.0f)
                    {
                        alpha = 1.0f;
                    }
                }
                
                if(alpha == 1.0f)
                {
                    PlayMusicStream(music);

                    rectext5.x = screenWidth;
                    rectext6.x = screenWidth;
                    framesCounter++;
                    
                    if(sound == true)
                    {
                       if(sonido == 0)
                       {
                           PlaySound(inicio); 
                           sonido++;
                       }
                    }
                    
                    if(framesCounter <= 40)
                    {
                        rec2.y = screenHeight - screenHeight/3;
                    }
                    if(framesCounter > 40)
                    {
                        rec2.y = screenHeight;
                    }
                    if(framesCounter == 80)
                    {
                        framesCounter = 0;
                    } 
                    
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = TITLE2;
                        framesCounter = 0;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                    }
                }  
            
            
                
            } break;
            
            case TITLE2: //IA O PVP -------------------------------------------------------------------------------------------------
            {
                framesCounter++;
                sonido = 0;
                
                if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(IsKeyPressed(KEY_DOWN))
                {                   
                    rec1.y += 40;
                    framesCounter = 0;
                    if(rec1.y >= 385)
                    {
                        rec1.y = 385;
                    }
                }
                if(IsKeyPressed(KEY_UP))
                {
                    rec1.y -= 40;
                    framesCounter = 0;
                    if(rec1.y <= 265)
                    {
                        rec1.y = 265;
                    }
                }
                if(framesCounter <= 40)
                {
                    rec1.x = screenWidth/2 - 58;
                }
                if(framesCounter > 40)
                {
                    rec1.x = screenWidth;
                }
                if(framesCounter == 80)
                {
                    framesCounter = 0;
                }
                
            if(rec1.y == 265)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = TITLE3;
                    rec1.y = 265;
                    framesCounter = 0;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            
            if(rec1.y == 305)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = GAMEPLAY2;
                    rec1.y = 265;
                    framesCounter = 0;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            
            if(rec1.y == 345)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = OPTIONS3;
                    reco.y = 155;
                    rec1.y = 265;
                    framesCounter = 0;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            
            if(rec1.y == 385)
            {
                if(IsKeyPressed(KEY_ENTER))
                {                   
                    return 0;
                }
            }
            
            } break;
            
            case TITLE3: //SELECCION IA -----------------------------------------------------------------------------------------
            {
              framesCounter++;
                sonido = 0;
                
                if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(IsKeyPressed(KEY_DOWN))
                {                   
                    rec1.y += 40;
                    framesCounter = 0;
                    if(rec1.y >= 385)
                    {
                        rec1.y = 385;
                    }
                }
                if(IsKeyPressed(KEY_UP))
                {
                    rec1.y -= 40;
                    framesCounter = 0;
                    if(rec1.y <= 265)
                    {
                        rec1.y = 265;
                    }
                }
                if(framesCounter <= 40)
                {
                    rec1.x = screenWidth/2 - 58;
                }
                if(framesCounter > 40)
                {
                    rec1.x = screenWidth;
                }
                if(framesCounter == 80)
                {
                    framesCounter = 0;
                }
                
            if(rec1.y == 265)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = GAMEPLAY;
                    rec1.y = 265;
                    framesCounter = 0;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            
            if(rec1.y == 305)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = SURVIVAL;
                    rec1.y = 265;
                    framesCounter = 0;
                    rec3.width = 38.33;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            
            if(rec1.y == 345)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = HISCORES;
                    reco.y = 155;
                    rec1.y = 265;
                    framesCounter = 0;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            
            if(rec1.y == 385)
            {
                if(IsKeyPressed(KEY_ENTER))
                {                   
                    screen = TITLE2;
                    rec1.y = 265;
                    framesCounter = 0;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
                
            } break;
            
            case GAMEPLAY: // VS IA -------------------------------------------------------------------------------------------------
            { 
                // Update GAMEPLAY screen data here!
                framesCounter2++;
                
                if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                
                if(framesCounter2 >= 200)
                {
                    recgame1.x = 50;
                    framesCounter2 = 300;
                }
                
                //Pause
                if(IsKeyPressed(KEY_P))
                {
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                    screen = PAUSE;
                }

                // Calcular la nueva posición de la pelota
                ball.x = ball.x + ballVelocity.x;
                ball.y = ball.y + ballVelocity.y;
                
                //Control pala1
                if (IsKeyDown(KEY_UP))
                {
                    pala1.y -= velocidad;
                    rec9.y -= velocidad;
                }
                if (IsKeyDown(KEY_DOWN))
                {
                    pala1.y += velocidad;
                    rec9.y += velocidad;
                } 
                
                //Limites de la pala 1
                if(pala1.y < linea2.y+linea2.height)
                {
                    pala1.y = linea2.y+linea2.height;
                    rec9.y = linea2.y+linea2.height;
                }else if(pala1.y>(screenHeight-pala1.height)){
                    pala1.y = screenHeight-pala1.height;
                    rec9.y = (screenHeight-pala1.height);
                }
                
                //Control pala2
                if (ball.x > limitesia)
                {
                    if(ballVelocity.y > 0)
                    {
                        pala2.y += velocidadia;
                        rec10.y = pala2.y;
                    }
                    if(ballVelocity.y < 0)
                    {
                        pala2.y -= velocidadia;
                        rec10.y = pala2.y;
                    }
                }                                            
               
                //Limites de la pala 2
                if(pala2.y < linea2.y+linea2.height)
                {
                    pala2.y = linea2.y+linea2.height;
                    rec10.y = linea2.y+linea2.height;
                }else if(pala2.y>(screenHeight-pala2.height)){
                    pala2.y = screenHeight-pala2.height;
                    rec10.y = screenHeight-pala2.height;
                }
                
                // Mirar si chocamos con las palas
                if(CheckCollisionCircleRec(ball,ballSize,pala1)){
                    ball.x = pala1.x + ballSize + pala1.width;
                    ballVelocity.x *= -1.1;
                    rec9.height += 7;
                    if(sound == true)
                    {
                        PlaySound(rebote);
                    }
                }
                if(rec9.height >= 70)
                {
                    rec9.height = 70;
                }
                
                // Mirar si chocamos con las palas IA
                if(CheckCollisionCircleRec(ball,ballSize,pala2)){
                    ballVelocity.x *= -1.1;
                    rec10.height += 7;
                    ball.x = pala2.x - ballSize - pala2.width;
                    if(sound == true)
                    {
                        PlaySound(rebote);
                    }
                }
                if(rec10.height >= 70)
                {
                    rec10.height = 70;
                }
                
                //Miro que no se pase en x (negativo)
                if((ball.x - ballSize) < 0){
                    ballVelocity.x = maxVelocity;
                    ball.x = ballSize;
                    rec3.width -= 38.34;
                    if(sound == true)
                    {
                        PlaySound(vida);
                    }
                }
                //Miro que no se pase en x (screenWidth)
                if((ball.x + ballSize) > screenWidth){
                    ball.x = screenWidth - ballSize;
                    ballVelocity.x = -maxVelocity;
                    rec6.width -= 38.34;
                    rec6.x += 38.34;
                    if(sound == true)
                    {
                        PlaySound(vida);
                    }
                }
                //Miro que no se pase en y (negativo)
                if((ball.y - ballSize) < (linea2.y+linea2.height)){
                    ball.y = ballSize + (linea2.y+linea2.height);
                    ballVelocity.y = maxVelocity;
                }
               //Miro que no se pase en y (screenHeight)
                if((ball.y + ballSize) > screenHeight){
                    ball.y = screenHeight- ballSize;
                    ballVelocity.y = -maxVelocity;
                }
                
                if(rec9.height == 70)
                {
                    if(IsKeyPressed(KEY_SPACE))
                    {
                        rec3.width += 38.34;
                        rec9.height = 0;
                        secondsCounter += 10;
                        if(sound == true)
                        {
                            PlaySound(powerup);
                        }
                    }
                }
                if(rec10.height == 70)
                {
                    rec6.width += 38.34;
                    rec6.x -= 38.34;
                    rec10.height = 0;
                    secondsCounter += 10;
                    if(sound == true)
                        {
                            PlaySound(powerup);
                        }
                }
                if(rec3.width >= 230)
                {
                    rec3.width = 230;
                }
                if(rec6.width >= 230)
                {
                    rec6.width = 230;
                }

                //Cronometro
                if(framesCounter == 60)
                {
                  secondsCounter--;
                  framesCounter = 0;
                }
                framesCounter++;
                if(secondsCounter == 0)
                {
                    screen = ENDING3;
                    rec3.width = 230;
                    rec6.width = 230;
                    rec6.x = screenWidth/2 + 45;
                    secondsCounter = 99;
                    pala1.y = screenHeight/2 - pala2.height/2 + 15;
                    pala2.y = screenHeight/2 - pala2.height/2 + 15;
                    ball.x = screenWidth/2 - ballSize/2;
                    ball.y = screenHeight/2 - ballSize/2;
                }
                
                //Final WIN/LOSE
                if(rec3.width <= 0)
                {
                    screen = ENDING2;
                    rec3.width = 230;
                    rec6.width = 230;
                    rec6.x = screenWidth/2 + 45;
                    secondsCounter = 99;
                    pala1.y = screenHeight/2 - pala1.height/2 + 15;
                    pala2.y = screenHeight/2 - pala1.height/2 + 15;
                    ball.x = screenWidth/2 - ballSize/2;
                    ball.y = screenHeight/2 - ballSize/2;
                }
                if(rec6.width <= 0)
                {
                    screen = ENDING;
                    rec3.width = 230;
                    rec6.width = 230;
                    rec6.x = screenWidth/2 + 45;
                    secondsCounter = 99;
                    pala1.y = screenHeight/2 - pala1.height/2 + 15;
                    pala2.y = screenHeight/2 - pala1.height/2 + 15;
                    ball.x = screenWidth/2 - ballSize/2;
                    ball.y = screenHeight/2 - ballSize/2;
                }
                
            } break;
            
            case GAMEPLAY2: //VS PLAYER -----------------------------------------------------------------------------------------------
            { 
                // Update GAMEPLAY2 screen data here!
                framesCounter2++;
                
                if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                
                if(framesCounter2 >= 200)
                {
                    recgame1.x = 50;
                    recgame2.x = screenWidth - 200;
                    framesCounter2 = 300;
                }
                
                //Pause
                if(IsKeyPressed(KEY_P))
                {
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                    screen = PAUSE2;
                }

                // Calcular la nueva posición de la pelota
                ball.x = ball.x + ballVelocity.x;
                ball.y = ball.y + ballVelocity.y;
                
                //Control pala1
                if (IsKeyDown(KEY_W))
                {
                    pala1.y -= velocidad;
                    rec9.y -= velocidad;
                }
                if (IsKeyDown(KEY_S))
                {
                    pala1.y += velocidad;
                    rec9.y += velocidad;
                } 
                
                //Limites de la pala 1
                if(pala1.y < linea2.y+linea2.height)
                {
                    pala1.y = linea2.y+linea2.height;
                    rec9.y = linea2.y+linea2.height;
                }else if(pala1.y>(screenHeight-pala1.height)){
                    pala1.y = screenHeight-pala1.height;
                    rec9.y = (screenHeight-pala1.height);
                }
                
                //Control pala2
                if (IsKeyDown(KEY_UP))
                {
                    pala2.y -= velocidad2;
                    rec11.y -= velocidad2;
                }
                if (IsKeyDown(KEY_DOWN))
                {
                    pala2.y += velocidad2;
                    rec11.y += velocidad2;
                }                                             
               
                //Limites de la pala 2
                if(pala2.y < linea2.y+linea2.height)
                {
                    pala2.y = linea2.y+linea2.height;
                    rec11.y = linea2.y+linea2.height;
                }else if(pala2.y>(screenHeight-pala2.height)){
                    pala2.y = screenHeight-pala2.height;
                    rec11.y = screenHeight-pala2.height;
                }
                
                // Mirar si chocamos con las palas
                if(CheckCollisionCircleRec(ball,ballSize,pala1)){
                    ball.x = pala1.x + ballSize + pala1.width;
                    ballVelocity.x *= -1.1;
                    rec9.height += 7;
                    if(sound == true)
                    {
                        PlaySound(rebote);
                    }
                }
                if(rec9.height >= 70)
                {
                    rec9.height = 70;
                }
                
                // Mirar si chocamos con las palas IA
                if(CheckCollisionCircleRec(ball,ballSize,pala2)){
                    ballVelocity.x *= -1.1;
                    rec11.height += 7;
                    ball.x = pala2.x - ballSize - pala2.width;
                    if(sound == true)
                    {
                        PlaySound(rebote);
                    }
                }
                if(rec11.height >= 70)
                {
                    rec11.height = 70;
                }
                
                //Miro que no se pase en x (negativo)
                if((ball.x - ballSize) < 0){
                    ballVelocity.x = maxVelocity;
                    ball.x = ballSize;
                    rec3.width -= 38.34;
                    if(sound == true)
                    {
                        PlaySound(vida);
                    }
                }
                //Miro que no se pase en x (screenWidth)
                if((ball.x + ballSize) > screenWidth){
                    ball.x = screenWidth - ballSize;
                    ballVelocity.x = -maxVelocity;
                    rec6.width -= 38.34;
                    rec6.x += 38.34;
                    if(sound == true)
                    {
                        PlaySound(vida);
                    }
                }
                //Miro que no se pase en y (negativo)
                if((ball.y - ballSize) < (linea2.y+linea2.height)){
                    ball.y = ballSize + (linea2.y+linea2.height);
                    ballVelocity.y = maxVelocity;
                }
               //Miro que no se pase en y (screenHeight)
                if((ball.y + ballSize) > screenHeight){
                    ball.y = screenHeight- ballSize;
                    ballVelocity.y = -maxVelocity;
                }
                
                if(rec9.height == 70)
                {
                    if(IsKeyPressed(KEY_SPACE))
                    {
                        rec3.width += 38.34;
                        rec9.height = 0;
                        secondsCounter += 10;
                        if(sound == true)
                        {
                            PlaySound(powerup);
                        }
                    }
                }
                if(rec3.width >= 230)
                {
                    rec3.width = 230;
                }
                if(rec11.height == 70)
                {
                    if(IsKeyPressed(KEY_J))
                    {
                        rec6.width += 38.34;
                        rec6.x -= 38.34;
                        rec11.height = 0;
                        secondsCounter += 10;
                        if(sound == true)
                        {
                            PlaySound(powerup);
                        }
                    }
                }
                if(rec6.width >= 230)
                {
                    rec6.width = 230;
                }

                //Cronometro
                if(framesCounter == 60)
                {
                  secondsCounter--;
                  framesCounter = 0;
                }
                framesCounter++;
                if(secondsCounter == 0)
                {
                    screen = ENDING4;
                    rec3.width = 230;
                    rec6.width = 230;
                    rec6.x = screenWidth/2 + 45;
                    secondsCounter = 99;
                    pala1.y = screenHeight/2 - pala2.height/2 + 15;
                    pala2.y = screenHeight/2 - pala2.height/2 + 15;
                    ball.x = screenWidth/2 - ballSize/2;
                    ball.y = screenHeight/2 - ballSize/2;
                }

                //Final WIN/LOSE
                if(rec3.width <= 0)
                {
                    screen = ENDING6;
                    rec3.width = 230;
                    rec6.width = 230;
                    rec6.x = screenWidth/2 + 45;
                    secondsCounter = 99;
                    pala1.y = screenHeight/2 - pala1.height/2 + 15;
                    pala2.y = screenHeight/2 - pala1.height/2 + 15;
                    ball.x = screenWidth/2 - ballSize/2;
                    ball.y = screenHeight/2 - ballSize/2;
                }
                if(rec6.width <= 0)
                {
                    screen = ENDING5;
                    rec3.width = 230;
                    rec6.width = 230;
                    rec6.x = screenWidth/2 + 45;
                    secondsCounter = 99;
                    pala1.y = screenHeight/2 - pala1.height/2 + 15;
                    pala2.y = screenHeight/2 - pala1.height/2 + 15;
                    ball.x = screenWidth/2 - ballSize/2;
                    ball.y = screenHeight/2 - ballSize/2;
                }
                
            } break;
            
            case SURVIVAL: // SURVIVAL ---------------------------------------------------------------------------------------------
            { 
                // Update GAMEPLAY screen data here!
                framesCounter2++;
                
                if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                
                if(framesCounter2 >= 200)
                {
                    recgame1.x = 50;
                    framesCounter2 = 300;
                }
                
                //Pause
                if(IsKeyPressed(KEY_P))
                {
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                    screen = PAUSE3;
                }

                // Calcular la nueva posición de la pelota
                ball.x = ball.x + ballVelocity.x;
                ball.y = ball.y + ballVelocity.y;
                
                //Control pala1
                if (IsKeyDown(KEY_UP))
                {
                    pala1.y -= velocidad;
                    rec9.y -= velocidad;
                }
                if (IsKeyDown(KEY_DOWN))
                {
                    pala1.y += velocidad;
                    rec9.y += velocidad;
                } 
                
                //Limites de la pala 1
                if(pala1.y < linea2.y+linea2.height)
                {
                    pala1.y = linea2.y+linea2.height;
                    rec9.y = linea2.y+linea2.height;
                }else if(pala1.y>(screenHeight-pala1.height)){
                    pala1.y = screenHeight-pala1.height;
                    rec9.y = (screenHeight-pala1.height);
                }
                
                //Control pala3
                if (ball.x > limitesia)
                {
                    if(ballVelocity.y > 0)
                    {
                        pala3.y += velocidadia;
                        rec10.y = pala3.y;
                    }
                    if(ballVelocity.y < 0)
                    {
                        pala3.y -= velocidadia;
                        rec10.y = pala3.y;
                    }
                }                                            
               
                //Limites de la pala 2
                if(pala3.y < linea2.y+linea2.height)
                {
                    pala3.y = linea2.y+linea2.height;
                }else if(pala3.y>(screenHeight-pala3.height)){
                    pala3.y = screenHeight-pala3.height;
                }
                
                // Mirar si chocamos con las palas
                if(CheckCollisionCircleRec(ball,ballSize,pala1)){
                    ball.x = pala1.x + ballSize + pala1.width;
                    ballVelocity.x *= -1.1;
                    rec9.height += 7;
                    choques += 1;
                    puntuacion += 5;
                    if(sound == true)
                    {
                        PlaySound(rebote);
                    }
                }
                if(rec9.height >= 70)
                {
                    rec9.height = 70;
                }
                
                // Mirar si chocamos con las palas IA
                if(CheckCollisionCircleRec(ball,ballSize,pala3)){
                    ballVelocity.x *= -1.1;
                    ball.x = pala3.x - ballSize - pala3.width;
                    if(sound == true)
                    {
                        PlaySound(rebote);
                    }
                }

                
                //Miro que no se pase en x (negativo)
                if((ball.x - ballSize) < 0){
                    ballVelocity.x = maxVelocity;
                    ball.x = ballSize;
                    rec3.width -= 38.34;
                    if(sound == true)
                    {
                        PlaySound(vida);
                    }
                }
                //Miro que no se pase en x (screenWidth)
                if((ball.x + ballSize) > screenWidth){
                    ball.x = screenWidth - ballSize;
                    ballVelocity.x = -maxVelocity;
                }
                //Miro que no se pase en y (negativo)
                if((ball.y - ballSize) < (linea2.y+linea2.height)){
                    ball.y = ballSize + (linea2.y+linea2.height);
                    ballVelocity.y = maxVelocity;
                }
               //Miro que no se pase en y (screenHeight)
                if((ball.y + ballSize) > screenHeight){
                    ball.y = screenHeight- ballSize;
                    ballVelocity.y = -maxVelocity;
                }
                
                puntuacionfinal = puntuacion + choques;
                
                if(rec9.height == 70)
                {
                    if(IsKeyPressed(KEY_SPACE))
                    {
                        rec9.height = 0;
                        puntuacion += 10;
                        if(sound == true)
                        {
                            PlaySound(powerup);
                        }
                    }
                }
                
                if(rec3.width >= 230)
                {
                    rec3.width = 230;
                }
                
                //Final WIN/LOSE
                if(rec3.width <= 0)
                {
                    screen = ENDING7;
                    rec3.width = 230;
                    rec6.width = 230;
                    rec6.x = screenWidth/2 + 45;
                    secondsCounter = 99;
                    pala1.y = screenHeight/2 - pala1.height/2 + 15;
                    pala3.y = screenHeight/2 - pala1.height/2 + 15;
                    ball.x = screenWidth/2 - ballSize/2;
                    ball.y = screenHeight/2 - ballSize/2;
                    
                //PUNTUACION 3
                if(puntuacionfinal >= puntuacion3)
                {
                    if(puntuacionfinal < puntuacion2)
                    {
                        if(puntuacionfinal < puntuacion1)
                        {
                            puntuacion3 = puntuacionfinal;
                        }
                    }
                }
                //PUNTUACION 2
                if(puntuacionfinal > puntuacion3)
                {
                    if(puntuacionfinal >= puntuacion2)
                    {
                        if(puntuacionfinal < puntuacion1)
                        {
                           puntuacion3 = puntuacion2;
                           puntuacion2 = puntuacionfinal;
                        }
                    }
                }
                //PUNTUACION 1
                if(puntuacionfinal > puntuacion3)
                {
                    if(puntuacionfinal > puntuacion2)
                    {
                        if(puntuacionfinal > puntuacion1)
                        {
                            puntuacion3 = puntuacion2;
                            puntuacion2 = puntuacion1;
                            puntuacion1 = puntuacionfinal;
                        }
                    }
                }
                }                
            } break;
            
            case HISCORES: // --------------------------------------------------------------------------------------------------
            {
                if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = TITLE3;
                    reco.y = 155;
                    rec1.y = 265;
                    framesCounter = 0;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            } break;
            
            case OPTIONS: // ---------------------------------------------------------------------------------------------------
            {            
            if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(reco.y == 155)
                {
                    if(IsKeyPressed(KEY_RIGHT))
                    {
                        recs.x = screenWidth/2 + 65;
                        sound = false;
                    }
                    if(IsKeyPressed(KEY_LEFT))
                    {
                        recs.x = screenWidth/2 - 118;
                        sound = true;
                    }  
                }
                if(reco.y == 255)
                {
                    if(recd.x == 165)
                    {
                        velocidadia = 4;
                        limitesia = screenWidth/2;
                    }
                    if(recd.x == 338)
                    {
                        velocidadia = 4;
                        limitesia = screenWidth/4;
                    }
                    if(recd.x == 511)
                    {
                        velocidadia = 5;
                        limitesia = screenWidth/8;
                    }
                    if(IsKeyPressed(KEY_RIGHT))
                    {
                        recd.x += 173;
                        if(recd.x >= 511)
                        {
                            recd.x = 511;
                        }
                    }
                    if(IsKeyPressed(KEY_LEFT))
                    {
                        recd.x -= 173;
                        if(recd.x <= 165)
                        {
                            recd.x = 165;
                        }
                    }  
                }
                if(IsKeyPressed(KEY_DOWN))
                {                   
                    reco.y += 100;
                    if(reco.y >= 355)
                    {
                        reco.y = 355;
                    }
                }
                if(IsKeyPressed(KEY_UP))
                {
                    reco.y -= 100;
                    if(reco.y <= 155)
                    {
                        reco.y = 155;
                    }
                }
                
                if(reco.y == 355)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = PAUSE;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                        reco.y = 155;
                    }
                }
                          
            } break;
            
            case OPTIONS2: // ---------------------------------------------------------------------------------------------------
            {            
            if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(reco.y == 155)
                {
                    if(IsKeyPressed(KEY_RIGHT))
                    {
                        recs.x = screenWidth/2 + 65;
                        sound = false;
                    }
                    if(IsKeyPressed(KEY_LEFT))
                    {
                        recs.x = screenWidth/2 - 118;
                        sound = true;
                    }  
                }
                if(reco.y == 255)
                {
                    if(recd.x == 165)
                    {
                        velocidadia = 4;
                        limitesia = screenWidth/2;
                    }
                    if(recd.x == 338)
                    {
                        velocidadia = 4;
                        limitesia = screenWidth/4;
                    }
                    if(recd.x == 511)
                    {
                        velocidadia = 5;
                        limitesia = screenWidth/8;
                    }
                    if(IsKeyPressed(KEY_RIGHT))
                    {
                        recd.x += 173;
                        if(recd.x >= 511)
                        {
                            recd.x = 511;
                        }
                    }
                    if(IsKeyPressed(KEY_LEFT))
                    {
                        recd.x -= 173;
                        if(recd.x <= 165)
                        {
                            recd.x = 165;
                        }
                    }  
                }
                if(IsKeyPressed(KEY_DOWN))
                {                   
                    reco.y += 100;
                    if(reco.y >= 355)
                    {
                        reco.y = 355;
                    }
                }
                if(IsKeyPressed(KEY_UP))
                {
                    reco.y -= 100;
                    if(reco.y <= 155)
                    {
                        reco.y = 155;
                    }
                }
                
                if(reco.y == 355)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = PAUSE2;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                        reco.y = 155;
                    }
                }
            } break;
            
            case OPTIONS3: // ---------------------------------------------------------------------------------------------------
            {            
            if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(reco.y == 155)
                {
                    if(IsKeyPressed(KEY_RIGHT))
                    {
                        recs.x = screenWidth/2 + 65;
                        sound = false;
                    }
                    if(IsKeyPressed(KEY_LEFT))
                    {
                        recs.x = screenWidth/2 - 118;
                        sound = true;
                    }  
                }
                if(reco.y == 255)
                {
                    if(recd.x == 165)
                    {
                        velocidadia = 4;
                        limitesia = screenWidth/2;
                    }
                    if(recd.x == 338)
                    {
                        velocidadia = 4;
                        limitesia = screenWidth/4;
                    }
                    if(recd.x == 511)
                    {
                        velocidadia = 5;
                        limitesia = screenWidth/8;
                    }
                    if(IsKeyPressed(KEY_RIGHT))
                    {
                        recd.x += 173;
                        if(recd.x >= 511)
                        {
                            recd.x = 511;
                        }
                    }
                    if(IsKeyPressed(KEY_LEFT))
                    {
                        recd.x -= 173;
                        if(recd.x <= 165)
                        {
                            recd.x = 165;
                        }
                    }  
                }
                if(IsKeyPressed(KEY_DOWN))
                {                   
                    reco.y += 100;
                    if(reco.y >= 355)
                    {
                        reco.y = 355;
                    }
                }
                if(IsKeyPressed(KEY_UP))
                {
                    reco.y -= 100;
                    if(reco.y <= 155)
                    {
                        reco.y = 155;
                    }
                }
                
                if(reco.y == 355)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = TITLE2;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                        reco.y = 155;
                    }
                }
            } break;
            
            case OPTIONS4: // ---------------------------------------------------------------------------------------------------
            {            
            if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(reco.y == 155)
                {
                    if(IsKeyPressed(KEY_RIGHT))
                    {
                        recs.x = screenWidth/2 + 65;
                        sound = false;
                    }
                    if(IsKeyPressed(KEY_LEFT))
                    {
                        recs.x = screenWidth/2 - 118;
                        sound = true;
                    }  
                }
                if(reco.y == 255)
                {
                    if(recd.x == 165)
                    {
                        velocidadia = 4;
                        limitesia = screenWidth/2;
                    }
                    if(recd.x == 338)
                    {
                        velocidadia = 4;
                        limitesia = screenWidth/4;
                    }
                    if(recd.x == 511)
                    {
                        velocidadia = 5;
                        limitesia = screenWidth/8;
                    }
                    if(IsKeyPressed(KEY_RIGHT))
                    {
                        recd.x += 173;
                        if(recd.x >= 511)
                        {
                            recd.x = 511;
                        }
                    }
                    if(IsKeyPressed(KEY_LEFT))
                    {
                        recd.x -= 173;
                        if(recd.x <= 165)
                        {
                            recd.x = 165;
                        }
                    }  
                }
                if(IsKeyPressed(KEY_DOWN))
                {                   
                    reco.y += 100;
                    if(reco.y >= 355)
                    {
                        reco.y = 355;
                    }
                }
                if(IsKeyPressed(KEY_UP))
                {
                    reco.y -= 100;
                    if(reco.y <= 155)
                    {
                        reco.y = 155;
                    }
                }
                
                if(reco.y == 355)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = PAUSE3;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                        reco.y = 155;
                    }
                }
                          
            } break;
            
            case PAUSE: //PAUSE MENU -------------------------------------------------------------------------------------------------
            { 
            framesCounter++;
            
            if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(IsKeyPressed(KEY_DOWN))
                {                   
                    recp.y += 40;
                    framesCounter = 0;
                    if(recp.y >= 337)
                    {
                        recp.y = 337;
                    }
                }
                if(IsKeyPressed(KEY_UP))
                {
                    recp.y -= 40;
                    framesCounter = 0;
                    if(recp.y <= 217)
                    {
                        recp.y = 217;
                    }
                }
                if(framesCounter <= 40)
                {
                    recp.x = screenWidth/2 - 72;
                }
                if(framesCounter > 40)
                {
                    recp.x = screenWidth;
                }
                if(framesCounter == 80)
                {
                    framesCounter = 0;
                }
                
              if(recp.y == 217)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = GAMEPLAY;
                    recp.y = 217;
                    framesCounter = 0;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            
            if(recp.y == 257)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = GAMEPLAY;
                    pala1.y = screenHeight/2 - pala1.height/2 + 15;
                    pala2.y = screenHeight/2 - pala1.height/2 + 15;
                    recp.y = 217;
                    ball.x = screenWidth/2 - ballSize/2;
                    ball.y = screenHeight/2 - ballSize/2;
                    framesCounter = 0;
                    framesCounter2 = 0;
                    recgame1.x = screenWidth;
                    secondsCounter = 99;
                    rec3.width = 230;
                    rec6.width = 230;
                    rec6.x = screenWidth/2 + 45;
                    rec9.height = 0;
                    rec10.height = 0;
                    rec9.y = screenHeight/2 - pala1.height/2 + 15;
                    ballVelocity.x = maxVelocity;
                    ballVelocity.y = maxVelocity;
                    velocidad = 4.5;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            
            if(recp.y == 297)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = OPTIONS;
                    reco.y = 155;
                    recp.y = 217;
                    framesCounter = 0;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            
            if(recp.y == 337)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = TITLE2;
                    pala1.y = screenHeight/2 - pala1.height/2 + 15;
                    pala2.y = screenHeight/2 - pala1.height/2 + 15;
                    recp.y = 217;
                    framesCounter = 0;
                    framesCounter2 = 0;
                    recgame1.x = screenWidth;
                    secondsCounter = 99;
                    ball.x = screenWidth/2 - ballSize/2;
                    ball.y = screenHeight/2 - ballSize/2;
                    rec3.width = 230;
                    rec6.width = 230;
                    rec6.x = screenWidth/2 + 45;
                    rec9.height = 0;
                    rec10.height = 0;
                    rec9.y = screenHeight/2 - pala1.height/2 + 15;
                    ballVelocity.x = maxVelocity;
                    ballVelocity.y = maxVelocity;
                    velocidad = 4.5;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
              
            } break;
            
            case PAUSE2: //PAUSE MENU -------------------------------------------------------------------------------------------------
            { 
            framesCounter++;
            
            if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(IsKeyPressed(KEY_DOWN))
                {                   
                    recp.y += 40;
                    framesCounter = 0;
                    if(recp.y >= 337)
                    {
                        recp.y = 337;
                    }
                }
                if(IsKeyPressed(KEY_UP))
                {
                    recp.y -= 40;
                    framesCounter = 0;
                    if(recp.y <= 217)
                    {
                        recp.y = 217;
                    }
                }
                if(framesCounter <= 40)
                {
                    recp.x = screenWidth/2 - 72;
                }
                if(framesCounter > 40)
                {
                    recp.x = screenWidth;
                }
                if(framesCounter == 80)
                {
                    framesCounter = 0;
                }
                
               if(recp.y == 217)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = GAMEPLAY2;
                    recp.y = 217;
                    framesCounter = 0;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            
            if(recp.y == 257)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = GAMEPLAY2;
                    pala1.y = screenHeight/2 - pala1.height/2 + 15;
                    pala2.y = screenHeight/2 - pala1.height/2 + 15;
                    recp.y = 217;
                    framesCounter = 0;
                    framesCounter2 = 0;
                    recgame1.x = screenWidth;
                    recgame2.x = screenWidth;
                    secondsCounter = 99;
                    ball.x = screenWidth/2 - ballSize/2;
                    ball.y = screenHeight/2 - ballSize/2;
                    rec3.width = 230;
                    rec6.width = 230;
                    rec6.x = screenWidth/2 + 45;
                    rec9.height = 0;
                    rec11.height = 0;
                    rec9.y = screenHeight/2 - pala1.height/2 + 15;
                    rec11.y = screenHeight/2 - pala1.height/2 + 15;
                    ballVelocity.x = maxVelocity;
                    ballVelocity.y = maxVelocity;
                    velocidad = 4.5;
                    velocidad2 = 4.5;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            
            if(recp.y == 297)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = OPTIONS2;
                    reco.y = 155;
                    recp.y = 217;
                    framesCounter = 0;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            
            if(recp.y == 337)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = TITLE2;
                    pala1.y = screenHeight/2 - pala1.height/2 + 15;
                    pala2.y = screenHeight/2 - pala1.height/2 + 15;
                    recp.y = 217;
                    framesCounter = 0;
                    framesCounter2 = 0;
                    recgame1.x = screenWidth;
                    recgame2.x = screenWidth;
                    secondsCounter = 99;
                    ball.x = screenWidth/2 - ballSize/2;
                    ball.y = screenHeight/2 - ballSize/2;
                    rec3.width = 230;
                    rec6.width = 230;
                    rec6.x = screenWidth/2 + 45;
                    rec9.height = 0;
                    rec11.height = 0;
                    rec9.y = screenHeight/2 - pala1.height/2 + 15;
                    rec11.y = screenHeight/2 - pala1.height/2 + 15;
                    ballVelocity.x = maxVelocity;
                    ballVelocity.y = maxVelocity;
                    velocidad = 4.5;
                    velocidad2 = 4.5;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            } break;
            
            case PAUSE3: //PAUSE MENU -------------------------------------------------------------------------------------------------
            { 
            framesCounter++;
            
            if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(IsKeyPressed(KEY_DOWN))
                {                   
                    recp.y += 40;
                    framesCounter = 0;
                    if(recp.y >= 337)
                    {
                        recp.y = 337;
                    }
                }
                if(IsKeyPressed(KEY_UP))
                {
                    recp.y -= 40;
                    framesCounter = 0;
                    if(recp.y <= 217)
                    {
                        recp.y = 217;
                    }
                }
                if(framesCounter <= 40)
                {
                    recp.x = screenWidth/2 - 72;
                }
                if(framesCounter > 40)
                {
                    recp.x = screenWidth;
                }
                if(framesCounter == 80)
                {
                    framesCounter = 0;
                }
                
              if(recp.y == 217)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = SURVIVAL;
                    recp.y = 217;
                    framesCounter = 0;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            
            if(recp.y == 257)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = SURVIVAL;
                    pala1.y = screenHeight/2 - pala1.height/2 + 15;
                    pala3.y = screenHeight/2 - pala1.height/2 + 15;
                    recp.y = 217;
                    ball.x = screenWidth/2 - ballSize/2;
                    ball.y = screenHeight/2 - ballSize/2;
                    framesCounter = 0;
                    framesCounter2 = 0;
                    puntuacionfinal = 0;
                    choques = 0;
                    puntuacion = 0;
                    recgame1.x = screenWidth;
                    secondsCounter = 99;
                    rec3.width = 230;
                    rec6.width = 230;
                    rec6.x = screenWidth/2 + 45;
                    rec9.height = 0;
                    rec10.height = 0;
                    rec9.y = screenHeight/2 - pala1.height/2 + 15;
                    ballVelocity.x = maxVelocity;
                    ballVelocity.y = maxVelocity;
                    velocidad = 4.5;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            
            if(recp.y == 297)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = OPTIONS4;
                    reco.y = 155;
                    recp.y = 217;
                    framesCounter = 0;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
            
            if(recp.y == 337)
            {
                if(IsKeyPressed(KEY_ENTER))
                {
                    screen = TITLE2;
                    pala1.y = screenHeight/2 - pala1.height/2 + 15;
                    pala3.y = screenHeight/2 - pala1.height/2 + 15;
                    recp.y = 217;
                    framesCounter = 0;
                    framesCounter2 = 0;
                    puntuacionfinal = 0;
                    choques = 0;
                    puntuacion = 0;
                    recgame1.x = screenWidth;
                    secondsCounter = 99;
                    ball.x = screenWidth/2 - ballSize/2;
                    ball.y = screenHeight/2 - ballSize/2;
                    rec3.width = 230;
                    rec6.width = 230;
                    rec6.x = screenWidth/2 + 45;
                    rec9.height = 0;
                    rec10.height = 0;
                    rec9.y = screenHeight/2 - pala1.height/2 + 15;
                    ballVelocity.x = maxVelocity;
                    ballVelocity.y = maxVelocity;
                    velocidad = 4.5;
                    if(sound == true)
                    { 
                        PlaySound(seleccion);                    
                    }
                }
            }
              
            } break;
            
            case ENDING: //WIN --------------------------------------------------------------------------------------------------
            {
                if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(IsKeyPressed(KEY_DOWN))
                {
                    rece.y += 50;
                }
                if(IsKeyPressed(KEY_UP))
                {
                    rece.y -= 50;
                }
                if(rece.y >= 246)
                {
                    rece.y = 246;
                }
                if(rece.y <= 196)
                {
                    rece.y = 196;
                }
                
                if(rece.y == 196)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = GAMEPLAY;
                        pala1.y = screenHeight/2 - pala1.height/2 + 15;
                        pala2.y = screenHeight/2 - pala1.height/2 + 15;
                        recp.y = 217;
                        framesCounter = 0;
                        framesCounter2 = 0;
                        recgame1.x = screenWidth;
                        secondsCounter = 99;
                        ball.x = screenWidth/2 - ballSize/2;
                        ball.y = screenHeight/2 - ballSize/2;
                        rec3.width = 230;
                        rec6.width = 230;
                        rec6.x = screenWidth/2 + 45;
                        rec9.height = 0;
                        rec10.height = 0;
                        rec9.y = screenHeight/2 - pala1.height/2 + 15;
                        ballVelocity.x = maxVelocity;
                        ballVelocity.y = maxVelocity;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                    }
                }
                if(rece.y == 246)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = TITLE2;
                        pala1.y = screenHeight/2 - pala1.height/2 + 15;
                        pala2.y = screenHeight/2 - pala1.height/2 + 15;
                        recp.y = 217;
                        framesCounter = 0;
                        framesCounter2 = 0;
                        recgame1.x = screenWidth;
                        secondsCounter = 99;
                        ball.x = screenWidth/2 - ballSize/2;
                        ball.y = screenHeight/2 - ballSize/2;
                        rec3.width = 230;
                        rec6.width = 230;
                        rec6.x = screenWidth/2 + 45;
                        rec9.height = 0;
                        rec10.height = 0;
                        rec9.y = screenHeight/2 - pala1.height/2 + 15;
                        ballVelocity.x = maxVelocity;
                        ballVelocity.y = maxVelocity;
                        rece.y = 196;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                    }
                }                
            } break;
            
            case ENDING2: //LOSE -------------------------------------------------------------------------------------------------
            {
                if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(IsKeyPressed(KEY_DOWN))
                {
                    rece.y += 50;
                }
                if(IsKeyPressed(KEY_UP))
                {
                    rece.y -= 50;
                }
                if(rece.y >= 246)
                {
                    rece.y = 246;
                }
                if(rece.y <= 196)
                {
                    rece.y = 196;
                }
                
                if(rece.y == 196)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = GAMEPLAY;
                        pala1.y = screenHeight/2 - pala1.height/2 + 15;
                        pala2.y = screenHeight/2 - pala1.height/2 + 15;
                        recp.y = 217;
                        framesCounter = 0;
                        framesCounter2 = 0;
                        recgame1.x = screenWidth;
                        secondsCounter = 99;
                        ball.x = screenWidth/2 - ballSize/2;
                        ball.y = screenHeight/2 - ballSize/2;
                        rec3.width = 230;
                        rec6.width = 230;
                        rec6.x = screenWidth/2 + 45;
                        rec9.height = 0;
                        rec10.height = 0;
                        rec9.y = screenHeight/2 - pala1.height/2 + 15;
                        ballVelocity.x = maxVelocity;
                        ballVelocity.y = maxVelocity;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                    }
                }
                if(rece.y == 246)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = TITLE2;
                        pala1.y = screenHeight/2 - pala1.height/2 + 15;
                        pala2.y = screenHeight/2 - pala1.height/2 + 15;
                        recp.y = 217;
                        framesCounter = 0;
                        framesCounter2 = 0;
                        recgame1.x = screenWidth;
                        secondsCounter = 99;
                        ball.x = screenWidth/2 - ballSize/2;
                        ball.y = screenHeight/2 - ballSize/2;
                        rec3.width = 230;
                        rec6.width = 230;
                        rec6.x = screenWidth/2 + 45;
                        rec9.height = 0;
                        rec10.height = 0;
                        rec9.y = screenHeight/2 - pala1.height/2 + 15;
                        ballVelocity.x = maxVelocity;
                        ballVelocity.y = maxVelocity;
                        rece.y = 196;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                    }
                }                
            } break;
            
            case ENDING3: //DRAW -------------------------------------------------------------------------------------------------
            {
                if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(IsKeyPressed(KEY_DOWN))
                {
                    rece.y += 50;
                }
                if(IsKeyPressed(KEY_UP))
                {
                    rece.y -= 50;
                }
                if(rece.y >= 246)
                {
                    rece.y = 246;
                }
                if(rece.y <= 196)
                {
                    rece.y = 196;
                }
                
                if(rece.y == 196)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = GAMEPLAY;
                        pala1.y = screenHeight/2 - pala1.height/2 + 15;
                        pala2.y = screenHeight/2 - pala1.height/2 + 15;
                        recp.y = 217;
                        framesCounter = 0;
                        framesCounter2 = 0;
                        recgame1.x = screenWidth;
                        secondsCounter = 99;
                        ball.x = screenWidth/2 - ballSize/2;
                        ball.y = screenHeight/2 - ballSize/2;
                        rec3.width = 230;
                        rec6.width = 230;
                        rec6.x = screenWidth/2 + 45;
                        rec9.height = 0;
                        rec10.height = 0;
                        rec9.y = screenHeight/2 - pala1.height/2 + 15;
                        ballVelocity.x = maxVelocity;
                        ballVelocity.y = maxVelocity;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                    }
                }
                if(rece.y == 246)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = TITLE2;
                        pala1.y = screenHeight/2 - pala1.height/2 + 15;
                        pala2.y = screenHeight/2 - pala1.height/2 + 15;
                        recp.y = 217;
                        framesCounter = 0;
                        framesCounter2 = 0;
                        recgame1.x = screenWidth;
                        secondsCounter = 99;
                        ball.x = screenWidth/2 - ballSize/2;
                        ball.y = screenHeight/2 - ballSize/2;
                        rec3.width = 230;
                        rec6.width = 230;
                        rec6.x = screenWidth/2 + 45;
                        rec9.height = 0;
                        rec10.height = 0;
                        rec9.y = screenHeight/2 - pala1.height/2 + 15;
                        ballVelocity.x = maxVelocity;
                        ballVelocity.y = maxVelocity;
                        rece.y = 196;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                    }
                }
            } break;
            
            case ENDING4: //DRAW -------------------------------------------------------------------------------------------------
            {
                if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(IsKeyPressed(KEY_DOWN))
                {
                    rece.y += 50;
                }
                if(IsKeyPressed(KEY_UP))
                {
                    rece.y -= 50;
                }
                if(rece.y >= 246)
                {
                    rece.y = 246;
                }
                if(rece.y <= 196)
                {
                    rece.y = 196;
                }
                
                if(rece.y == 196)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = GAMEPLAY2;
                        pala1.y = screenHeight/2 - pala1.height/2 + 15;
                        pala2.y = screenHeight/2 - pala1.height/2 + 15;
                        recp.y = 217;
                        framesCounter = 0;
                        framesCounter2 = 0;
                        recgame1.x = screenWidth;
                        recgame2.x = screenWidth;
                        secondsCounter = 99;
                        ball.x = screenWidth/2 - ballSize/2;
                        ball.y = screenHeight/2 - ballSize/2;
                        rec3.width = 230;
                        rec6.width = 230;
                        rec6.x = screenWidth/2 + 45;
                        rec9.height = 0;
                        rec11.height = 0;
                        rec11.y = screenHeight/2 - pala1.height/2 + 15;
                        rec9.y = screenHeight/2 - pala1.height/2 + 15;
                        ballVelocity.x = maxVelocity;
                        ballVelocity.y = maxVelocity;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                    }
                }
                if(rece.y == 246)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = TITLE2;
                        pala1.y = screenHeight/2 - pala1.height/2 + 15;
                        pala2.y = screenHeight/2 - pala1.height/2 + 15;
                        recp.y = 217;
                        framesCounter = 0;
                        framesCounter2 = 0;
                        recgame1.x = screenWidth;
                        recgame2.x = screenWidth;
                        secondsCounter = 99;
                        ball.x = screenWidth/2 - ballSize/2;
                        ball.y = screenHeight/2 - ballSize/2;
                        rec3.width = 230;
                        rec6.width = 230;
                        rec6.x = screenWidth/2 + 45;
                        rec9.height = 0;
                        rec11.height = 0;
                        rec11.y = screenHeight/2 - pala1.height/2 + 15;
                        rec9.y = screenHeight/2 - pala1.height/2 + 15;
                        ballVelocity.x = maxVelocity;
                        ballVelocity.y = maxVelocity;
                        rece.y = 196;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                    }
                }
            } break;
            
            case ENDING5: //WIN --------------------------------------------------------------------------------------------------
            {
                if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(IsKeyPressed(KEY_DOWN))
                {
                    rece.y += 50;
                }
                if(IsKeyPressed(KEY_UP))
                {
                    rece.y -= 50;
                }
                if(rece.y >= 246)
                {
                    rece.y = 246;
                }
                if(rece.y <= 196)
                {
                    rece.y = 196;
                }
                
                if(rece.y == 196)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = GAMEPLAY2;
                        pala1.y = screenHeight/2 - pala1.height/2 + 15;
                        pala2.y = screenHeight/2 - pala1.height/2 + 15;
                        recp.y = 217;
                        framesCounter = 0;
                        framesCounter2 = 0;
                        recgame1.x = screenWidth;
                        recgame2.x = screenWidth;
                        secondsCounter = 99;
                        ball.x = screenWidth/2 - ballSize/2;
                        ball.y = screenHeight/2 - ballSize/2;
                        rec3.width = 230;
                        rec6.width = 230;
                        rec6.x = screenWidth/2 + 45;
                        rec9.height = 0;
                        rec11.height = 0;
                        rec11.y = screenHeight/2 - pala1.height/2 + 15;
                        rec9.y = screenHeight/2 - pala1.height/2 + 15;
                        ballVelocity.x = maxVelocity;
                        ballVelocity.y = maxVelocity;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                    }
                }
                if(rece.y == 246)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = TITLE2;
                        pala1.y = screenHeight/2 - pala1.height/2 + 15;
                        pala2.y = screenHeight/2 - pala1.height/2 + 15;
                        recp.y = 217;
                        framesCounter = 0;
                        framesCounter2 = 0;
                        recgame1.x = screenWidth;
                        recgame2.x = screenWidth;
                        secondsCounter = 99;
                        ball.x = screenWidth/2 - ballSize/2;
                        ball.y = screenHeight/2 - ballSize/2;
                        rec3.width = 230;
                        rec6.width = 230;
                        rec6.x = screenWidth/2 + 45;
                        rec9.height = 0;
                        rec11.height = 0;
                        rec11.y = screenHeight/2 - pala1.height/2 + 15;
                        rec9.y = screenHeight/2 - pala1.height/2 + 15;
                        ballVelocity.x = maxVelocity;
                        ballVelocity.y = maxVelocity;
                        rece.y = 196;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                    }
                }
            } break;
            
            case ENDING6: //WIN --------------------------------------------------------------------------------------------------
            {
                if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(IsKeyPressed(KEY_DOWN))
                {
                    rece.y += 50;
                }
                if(IsKeyPressed(KEY_UP))
                {
                    rece.y -= 50;
                }
                if(rece.y >= 246)
                {
                    rece.y = 246;
                }
                if(rece.y <= 196)
                {
                    rece.y = 196;
                }
                
                if(rece.y == 196)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = GAMEPLAY2;
                        pala1.y = screenHeight/2 - pala1.height/2 + 15;
                        pala2.y = screenHeight/2 - pala1.height/2 + 15;
                        recp.y = 217;
                        framesCounter = 0;
                        framesCounter2 = 0;
                        recgame1.x = screenWidth;
                        recgame2.x = screenWidth;
                        secondsCounter = 99;
                        ball.x = screenWidth/2 - ballSize/2;
                        ball.y = screenHeight/2 - ballSize/2;
                        rec3.width = 230;
                        rec6.width = 230;
                        rec6.x = screenWidth/2 + 45;
                        rec9.height = 0;
                        rec11.height = 0;
                        rec11.y = screenHeight/2 - pala1.height/2 + 15;
                        rec9.y = screenHeight/2 - pala1.height/2 + 15;
                        ballVelocity.x = maxVelocity;
                        ballVelocity.y = maxVelocity;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                    }
                }
                if(rece.y == 246)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = TITLE2;
                        pala1.y = screenHeight/2 - pala1.height/2 + 15;
                        pala2.y = screenHeight/2 - pala1.height/2 + 15;
                        recp.y = 217;
                        framesCounter = 0;
                        framesCounter2 = 0;
                        recgame1.x = screenWidth;
                        recgame2.x = screenWidth;
                        secondsCounter = 99;
                        ball.x = screenWidth/2 - ballSize/2;
                        ball.y = screenHeight/2 - ballSize/2;
                        rec3.width = 230;
                        rec6.width = 230;
                        rec6.x = screenWidth/2 + 45;
                        rec9.height = 0;
                        rec11.height = 0;
                        rec11.y = screenHeight/2 - pala1.height/2 + 15;
                        rec9.y = screenHeight/2 - pala1.height/2 + 15;
                        ballVelocity.x = maxVelocity;
                        ballVelocity.y = maxVelocity;
                        rece.y = 196;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                    }
                }
            } break;
            
            case ENDING7: //WIN --------------------------------------------------------------------------------------------------
            {
                if(sound == false)
                    {
                        PauseMusicStream(music);
                    }
                if(sound == true)
                    {
                        ResumeMusicStream(music);
                    }
                    
                if(IsKeyPressed(KEY_DOWN))
                {
                    rece.y += 50;
                }
                if(IsKeyPressed(KEY_UP))
                {
                    rece.y -= 50;
                }
                if(rece.y >= 246)
                {
                    rece.y = 246;
                }
                if(rece.y <= 196)
                {
                    rece.y = 196;
                }
                
                if(rece.y == 196)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = SURVIVAL;
                        pala1.y = screenHeight/2 - pala1.height/2 + 15;
                        pala3.y = screenHeight/2 - pala1.height/2 + 15;
                        recp.y = 217;
                        framesCounter = 0;
                        framesCounter2 = 0;
                        puntuacion = 0;
                        choques = 0;
                        puntuacionfinal = 0;
                        recgame1.x = screenWidth;
                        recgame2.x = screenWidth;
                        secondsCounter = 99;
                        ball.x = screenWidth/2 - ballSize/2;
                        ball.y = screenHeight/2 - ballSize/2;
                        rec3.width = 38.33;
                        rec6.width = 230;
                        rec6.x = screenWidth/2 + 45;
                        rec9.height = 0;
                        rec11.height = 0;
                        rec11.y = screenHeight/2 - pala1.height/2 + 15;
                        rec9.y = screenHeight/2 - pala1.height/2 + 15;
                        ballVelocity.x = maxVelocity;
                        ballVelocity.y = maxVelocity;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                    }
                }
                if(rece.y == 246)
                {
                    if(IsKeyPressed(KEY_ENTER))
                    {
                        screen = TITLE2;
                        pala1.y = screenHeight/2 - pala1.height/2 + 15;
                        pala3.y = screenHeight/2 - pala1.height/2 + 15;
                        recp.y = 217;
                        framesCounter = 0;
                        framesCounter2 = 0;
                        puntuacion = 0;
                        choques = 0;
                        puntuacionfinal = 0;
                        recgame1.x = screenWidth;
                        recgame2.x = screenWidth;
                        secondsCounter = 99;
                        ball.x = screenWidth/2 - ballSize/2;
                        ball.y = screenHeight/2 - ballSize/2;
                        rec3.width = 230;
                        rec6.width = 230;
                        rec6.x = screenWidth/2 + 45;
                        rec9.height = 0;
                        rec11.height = 0;
                        rec11.y = screenHeight/2 - pala1.height/2 + 15;
                        rec9.y = screenHeight/2 - pala1.height/2 + 15;
                        ballVelocity.x = maxVelocity;
                        ballVelocity.y = maxVelocity;
                        rece.y = 196;
                        if(sound == true)
                        { 
                            PlaySound(seleccion);                    
                        }
                    }
                }
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
        ClearBackground(RAYWHITE);
        
        switch(screen) 
        {
            case LOGO: // ------------------------------------------------------------------------------------------------------
            {
                DrawTexture(texture2, screenWidth/2 - texture2.width/2, 30, Fade(recColor, alpha));
                DrawText(FormatText("SKIP >>"), screenWidth - 100, screenHeight - 50, 25, GRAY);
                
            } break;
            
            case TITLE: //PRESS ENTER ------------------------------------------------------------------------------------------
            {
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                DrawRectangleRec(rectext5, RAYWHITE);
                DrawRectangleRec(rectext6, RAYWHITE);
                
                DrawTexture(texture, screenWidth/2 - texture.width/2, 30, Fade(recColor, alpha));
                DrawText(message, screenWidth/2 - 175, screenHeight - screenHeight/3, 25, BLACK);
                DrawRectangleRec(rec2, RAYWHITE); 
                
            } break;
            
            case TITLE2: //IA O PVP --------------------------------------------------------------------------------------------
            {
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                
                DrawRectangleRec(rec1,RED);
                DrawTexture(texture, screenWidth/2 - texture.width/2, 30, WHITE);
                DrawText(FormatText("VS IA"), screenWidth/2 - 50, 270, 25, BLACK);
                DrawText(FormatText("VS PLAYER"), screenWidth/2 - 50, 310, 25, BLACK);
                DrawText(FormatText("OPTIONS"), screenWidth/2 - 50, 350, 25, BLACK);
                DrawText(FormatText("EXIT"), screenWidth/2 - 50, 390, 25, BLACK);
                
                
            } break;
            
            case TITLE3: //SELECCION IA ---------------------------------------------------------------------------------------
            {
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                
                DrawRectangleRec(rec1,RED);
                DrawTexture(texture, screenWidth/2 - texture.width/2, 30, WHITE);
                DrawText(FormatText("NORMAL"), screenWidth/2 - 50, 270, 25, BLACK);
                DrawText(FormatText("SURVIVAL"), screenWidth/2 - 50, 310, 25, BLACK);
                DrawText(FormatText("HISCORES"), screenWidth/2 - 50, 350, 25, BLACK);
                DrawText(FormatText("BACK"), screenWidth/2 - 50, 390, 25, BLACK);
            } break;
            
            case GAMEPLAY: //VS IA --------------------------------------------------------------------------------------------
            {               
                DrawText(FormatText("[UP]"), 90, 140, 40, GRAY);
                DrawText(FormatText("[SPACE]"), 50, 230, 40, GRAY);
                DrawText(FormatText("[DOWN]"), 70, screenHeight - 130, 40, GRAY);
                DrawRectangleRec(recgame1, RAYWHITE);
                
                //Pinto tablero
                DrawCircleV(campo,campoSize,GRAY);
                DrawCircleV(campo2,campo2Size,RAYWHITE);
                DrawRectangleRec(linea,DARKGRAY);
                DrawRectangleRec(linea2,BLACK); 
                
                //Pinto Pala 1
                DrawRectangleRec(pala1,BLACK);
                DrawRectangleRec(rec9,GREEN);

                //Pinto Pala 2
                DrawRectangleRec(pala2,BLACK);
                DrawRectangleRec(rec10,PURPLE);
                
                //Pinto Pelota
                DrawCircleV(ball,ballSize,RED);
                
                DrawText(FormatText("[P]PAUSE"), screenWidth - 105, 10, 20, GRAY);
                DrawText(FormatText("%d", secondsCounter), screenWidth/2 - 15, 5, 30, BLACK);
                
                //Barra vida
                DrawRectangleRec(rec5,BLACK);
                DrawRectangleRec(rec4,BLACK); 
                DrawRectangleRec(rec7,DARKGREEN);       
                DrawRectangleRec(rec8,DARKPURPLE);     
                DrawRectangleRec(rec3,GREEN);
                DrawRectangleRec(rec6,PURPLE);
                
            } break;
            
            case GAMEPLAY2: // VS PLAYER -------------------------------------------------------------------------------------
            {           
                DrawText(FormatText("[UP]"), screenWidth - 180, 140, 40, GRAY);
                DrawText(FormatText("[J]"), screenWidth - 170, 230, 40, GRAY);
                DrawText(FormatText("[DOWN]"), screenWidth - 200, screenHeight - 130, 40, GRAY);
                DrawText(FormatText("[W]"), 100, 140, 40, GRAY);
                DrawText(FormatText("[SPACE]"), 50, 230, 40, GRAY);
                DrawText(FormatText("[S]"), 100, screenHeight - 130, 40, GRAY);
                DrawRectangleRec(recgame1, RAYWHITE);
                DrawRectangleRec(recgame2, RAYWHITE);
                
                //Pinto tablero
                DrawCircleV(campo,campoSize,GRAY);
                DrawCircleV(campo2,campo2Size,RAYWHITE);
                DrawRectangleRec(linea,DARKGRAY);
                DrawRectangleRec(linea2,BLACK);
                
                //Pinto Pala 1
                DrawRectangleRec(pala1,BLACK);
                DrawRectangleRec(rec9,GREEN);
                
                //Pinto Pala 2
                DrawRectangleRec(pala2,BLACK);
                DrawRectangleRec(rec11,PURPLE); 
                
                //Pinto Pelota
                DrawCircleV(ball,ballSize,RED);
                
                DrawText(FormatText("[P]PAUSE"), screenWidth - 105, 10, 20, GRAY);
                DrawText(FormatText("%d", secondsCounter), screenWidth/2 - 15, 5, 30, BLACK);
                
                //Barra vida
                DrawRectangleRec(rec5,BLACK);
                DrawRectangleRec(rec4,BLACK); 
                DrawRectangleRec(rec7,DARKGREEN);       
                DrawRectangleRec(rec8,DARKPURPLE);     
                DrawRectangleRec(rec3,GREEN);
                DrawRectangleRec(rec6,PURPLE);
                
            } break;
            
            case SURVIVAL: //SURVIVAL -----------------------------------------------------------------------------------------
            {               
                DrawText(FormatText("[UP]"), 90, 140, 40, GRAY);
                DrawText(FormatText("[SPACE]"), 50, 230, 40, GRAY);
                DrawText(FormatText("[DOWN]"), 70, screenHeight - 130, 40, GRAY);
                DrawRectangleRec(recgame1, RAYWHITE);
                
                //Pinto tablero
                DrawCircleV(campo,campoSize,GRAY);
                DrawCircleV(campo2,campo2Size,RAYWHITE);
                DrawRectangleRec(linea,DARKGRAY);
                DrawRectangleRec(linea2,BLACK); 
                
                //Pinto Pala 1
                DrawRectangleRec(pala1,BLACK);
                DrawRectangleRec(rec9,GREEN);

                //Pinto Pala3
                DrawRectangleRec(pala3,BLACK);
                
                //Pinto Pelota
                DrawCircleV(ball,ballSize,RED);
                
                DrawText(FormatText("[P]PAUSE"), screenWidth - 105, 10, 20, GRAY);
                DrawText(FormatText("SCORE = %d", puntuacion), screenWidth/2 + 80, 7, 25, BLACK);
                
                //Barra vida
                DrawRectangleRec(rec4,BLACK); 
                DrawRectangleRec(rec7,DARKGREEN);          
                DrawRectangleRec(rec3,GREEN);
                
            } break;
            
            case HISCORES: //------------------------------------------------------------------------------------------------
            {
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                
                DrawRectangleRec(rech,RED);
                DrawText(FormatText("HISCORES"), screenWidth/2 - 130, 100, 50, BLACK);
                DrawText(FormatText("%d", puntuacion1), screenWidth/2 - 50, 180, 40, BLACK);
                DrawText(FormatText("%d", puntuacion2), screenWidth/2 - 50, 240, 40, BLACK);
                DrawText(FormatText("%d", puntuacion3), screenWidth/2 - 50, 300, 40, BLACK);
                DrawText(FormatText("BACK"), screenWidth/2 - 55, 360, 30, BLACK);
                
            } break;
            
            case OPTIONS: // ------------------------------------------------------------------------------------------------
            {
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                
                DrawRectangleRec(reco,RED);
                DrawRectangleRec(recs,RED);
                DrawRectangleRec(recd,RED);
                DrawText(FormatText("OPTIONS"), screenWidth/2 - 105, 50, 50, BLACK);
                DrawText(FormatText("SOUNDS"), screenWidth/2 - 55, 160, 30, BLACK);
                DrawText(FormatText("ON           OFF"), screenWidth/2 - 105, 210, 30, BLACK);
                DrawText(FormatText("DIFFICULTY"), screenWidth/2 - 90, 260, 30, BLACK);
                DrawText(FormatText("EASY      MEDIUM      HARD"), screenWidth/2 - 210, 310, 30, BLACK);
                DrawText(FormatText("BACK"), screenWidth/2 - 35, 360, 30, BLACK);
                
            } break;
            
            case OPTIONS2: // ------------------------------------------------------------------------------------------------
            {
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                
                DrawRectangleRec(reco,RED);
                DrawRectangleRec(recs,RED);
                DrawRectangleRec(recd,RED);
                DrawText(FormatText("OPTIONS"), screenWidth/2 - 105, 50, 50, BLACK);
                DrawText(FormatText("SOUNDS"), screenWidth/2 - 55, 160, 30, BLACK);
                DrawText(FormatText("ON           OFF"), screenWidth/2 - 105, 210, 30, BLACK);
                DrawText(FormatText("DIFFICULTY"), screenWidth/2 - 90, 260, 30, BLACK);
                DrawText(FormatText("EASY      MEDIUM      HARD"), screenWidth/2 - 210, 310, 30, BLACK);
                DrawText(FormatText("BACK"), screenWidth/2 - 35, 360, 30, BLACK);
                
            } break;
            
            case OPTIONS3: // ------------------------------------------------------------------------------------------------
            {
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                
                DrawRectangleRec(reco,RED);
                DrawRectangleRec(recs,RED);
                DrawRectangleRec(recd,RED);
                DrawText(FormatText("OPTIONS"), screenWidth/2 - 105, 50, 50, BLACK);
                DrawText(FormatText("SOUNDS"), screenWidth/2 - 55, 160, 30, BLACK);
                DrawText(FormatText("ON           OFF"), screenWidth/2 - 105, 210, 30, BLACK);
                DrawText(FormatText("DIFFICULTY"), screenWidth/2 - 90, 260, 30, BLACK);
                DrawText(FormatText("EASY      MEDIUM      HARD"), screenWidth/2 - 210, 310, 30, BLACK);
                DrawText(FormatText("BACK"), screenWidth/2 - 35, 360, 30, BLACK);
                
            } break;
            
            case OPTIONS4: // ------------------------------------------------------------------------------------------------
            {
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                
                DrawRectangleRec(reco,RED);
                DrawRectangleRec(recs,RED);
                DrawRectangleRec(recd,RED);
                DrawText(FormatText("OPTIONS"), screenWidth/2 - 105, 50, 50, BLACK);
                DrawText(FormatText("SOUNDS"), screenWidth/2 - 55, 160, 30, BLACK);
                DrawText(FormatText("ON           OFF"), screenWidth/2 - 105, 210, 30, BLACK);
                DrawText(FormatText("DIFFICULTY"), screenWidth/2 - 90, 260, 30, BLACK);
                DrawText(FormatText("EASY      MEDIUM      HARD"), screenWidth/2 - 210, 310, 30, BLACK);
                DrawText(FormatText("BACK"), screenWidth/2 - 35, 360, 30, BLACK);
                
            } break;
            
            case PAUSE: //PAUSE MENU ---------------------------------------------------------------------------------------
            { 
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                
                DrawRectangleRec(recp,RED);
                DrawText(FormatText("PAUSE MENU"), screenWidth/2 - 150, 100, 50, BLACK);
                DrawText(FormatText("RESUME"), screenWidth/2 - 65, 220, 30, BLACK);
                DrawText(FormatText("RESTART"), screenWidth/2 - 65, 260, 30, BLACK);
                DrawText(FormatText("OPTIONS"), screenWidth/2 - 65, 300, 30, BLACK);
                DrawText(FormatText("EXIT"), screenWidth/2 - 65, 340, 30, BLACK);
            } break;
            
            case PAUSE2: //PAUSE MENU --------------------------------------------------------------------------------------
            { 
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                DrawRectangleRec(recp,RED);
                DrawText(FormatText("PAUSE MENU"), screenWidth/2 - 150, 100, 50, BLACK);
                DrawText(FormatText("RESUME"), screenWidth/2 - 65, 220, 30, BLACK);
                DrawText(FormatText("RESUME"), screenWidth/2 - 65, 260, 30, BLACK);
                DrawText(FormatText("OPTIONS"), screenWidth/2 - 65, 300, 30, BLACK);
                DrawText(FormatText("EXIT"), screenWidth/2 - 65, 340, 30, BLACK);
            } break;
            
            case PAUSE3: //PAUSE MENU --------------------------------------------------------------------------------------
            { 
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                DrawRectangleRec(recp,RED);
                DrawText(FormatText("PAUSE MENU"), screenWidth/2 - 150, 100, 50, BLACK);
                DrawText(FormatText("RESUME"), screenWidth/2 - 65, 220, 30, BLACK);
                DrawText(FormatText("RESUME"), screenWidth/2 - 65, 260, 30, BLACK);
                DrawText(FormatText("OPTIONS"), screenWidth/2 - 65, 300, 30, BLACK);
                DrawText(FormatText("EXIT"), screenWidth/2 - 65, 340, 30, BLACK);
            } break;
            
            case ENDING: //WIN ---------------------------------------------------------------------------------------------
            {
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                
                DrawRectangleRec(rece, RED);
                
                DrawText(FormatText("YOU WIN"), screenWidth/2 - 105, 50, 50, BLACK);
                DrawText(FormatText("RESTART"), screenWidth/2 - 70, 200, 30, BLACK);
                DrawText(FormatText("EXIT"), screenWidth/2 - 30, 250, 30, BLACK);
                
            } break;
            
            case ENDING2: //LOSE -------------------------------------------------------------------------------------------
            {
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                
                DrawRectangleRec(rece, RED);
                
                DrawText(FormatText("YOU LOSE"), screenWidth/2 - 120, 50, 50, BLACK);
                DrawText(FormatText("RESTART"), screenWidth/2 - 70, 200, 30, BLACK);
                DrawText(FormatText("EXIT"), screenWidth/2 - 30, 250, 30, BLACK);
                
            } break;
            
            case ENDING3: //DRAW -------------------------------------------------------------------------------------------
            {
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                
                DrawRectangleRec(rece, RED);
                
                DrawText(FormatText("DRAW GAME"), screenWidth/2 - 150, 100, 50, BLACK);
                DrawText(FormatText("RESTART"), screenWidth/2 - 70, 200, 30, BLACK);
                DrawText(FormatText("EXIT"), screenWidth/2 - 30, 250, 30, BLACK);
                
            } break;
            
            case ENDING4: //DRAW --------------------------------------------------------------------------------------------
            {
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                
                DrawRectangleRec(rece, RED);
                
                DrawText(FormatText("DRAW GAME"), screenWidth/2 - 150, 100, 50, BLACK);
                DrawText(FormatText("RESTART"), screenWidth/2 - 70, 200, 30, BLACK);
                DrawText(FormatText("EXIT"), screenWidth/2 - 30, 250, 30, BLACK);
                
            } break;
            
            case ENDING5: //P2 WIN-------------------------------------------------------------------------------------------
            {
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                
                DrawRectangleRec(rece, RED);
                
                DrawText(FormatText("P1 WIN"), screenWidth/2 - 70, 100, 50, BLACK);
                DrawText(FormatText("RESTART"), screenWidth/2 - 70, 200, 30, BLACK);
                DrawText(FormatText("EXIT"), screenWidth/2 - 30, 250, 30, BLACK);
                
            } break;
            
            case ENDING6: //P1 WIN -------------------------------------------------------------------------------------------
            {
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                
                DrawRectangleRec(rece, RED);
                
                DrawText(FormatText("P2 WIN"), screenWidth/2 - 70, 100, 50, BLACK);
                DrawText(FormatText("RESTART"), screenWidth/2 - 70, 200, 30, BLACK);
                DrawText(FormatText("EXIT"), screenWidth/2 - 30, 250, 30, BLACK);
                
            } break;
            
            case ENDING7: //P1 WIN -------------------------------------------------------------------------------------------
            {
                DrawRectangleRec(rectext, BLACK);
                DrawRectangleRec(rectext2, BLACK);
                DrawRectangleRec(rectext3, GREEN);
                DrawRectangleRec(rectext4, PURPLE);
                
                DrawRectangleRec(rece, RED);
                
                DrawText(FormatText("SCORE: %d", puntuacionfinal), 270, 100, 50, BLACK);
                DrawText(FormatText("RESTART"), screenWidth/2 - 70, 200, 30, BLACK);
                DrawText(FormatText("EXIT"), screenWidth/2 - 30, 250, 30, BLACK);
                
            } break;
            default: break;
        }
        
            DrawFPS(14, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadSound(inicio);
    UnloadSound(seleccion);
    UnloadSound(rebote);
    UnloadSound(moneda);
    UnloadSound(vida);
    UnloadSound(powerup);
    UnloadMusicStream(music);
    
    CloseAudioDevice();
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}